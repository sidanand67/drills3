const items = [
    {
        name: "Orange",
        available: true,
        contains: "Vitamin C",
    },
    {
        name: "Mango",
        available: true,
        contains: "Vitamin K, Vitamin C",
    },
    {
        name: "Pineapple",
        available: true,
        contains: "Vitamin A",
    },
    {
        name: "Raspberry",
        available: false,
        contains: "Vitamin B, Vitamin A",
    },
    {
        name: "Grapes",
        contains: "Vitamin D",
        available: false,
    },
];


function availableItems(){
    return items.filter(item => item.available); 
}

function vitaminCItems(){
    return items.filter(item => item.contains === 'Vitamin C'); 
}

function vitaminAItems(){
    return items.filter(item => item.contains.includes('Vitamin A')); 
}

function vitaminsGroup(){
    return items.reduce((acc, curr) => {
        let vitamins = curr.contains.split(','); 
        vitamins.forEach(temp => {
            let vitamin = temp.trim(); 
            if(acc[vitamin]){
                acc[vitamin].push(curr.name); 
            }
            else {
                acc[vitamin] = [curr.name]; 
            }
        }); 
        return acc; 
    }, {}); 
}

function sortItems(){
    items.sort((a,b) => b.contains.length - a.contains.length); 
}

// Function Calls
let availableItemsList = availableItems(); 
console.log(availableItemsList); 

let vitaminCItemsList = vitaminCItems(); 
console.log(vitaminCItemsList); 

let vitaminAItemsList = vitaminAItems(); 
console.log(vitaminAItemsList); 

let vitaminsGroupList = vitaminsGroup(); 
console.log(vitaminsGroupList); 

sortItems(); 
console.log(items); 